import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FooterOnlyLayoutComponent } from './footer-only-layout/footer-only-layout.component';
import { MainLayoutComponent } from './main-layout/main-layout.component';
import { LoginComponent } from './login/login.component'
import { AuthGuard } from './auth.guard';
import { DashboardComponent } from './dashboard/dashboard.component'
import { RegisterComponent } from './register/register.component'



const routes: Routes = [{
               path:'',
               component:FooterOnlyLayoutComponent,
               children:[
                 {path:'',component: LoginComponent}
                ],
               canActivate:[AuthGuard]
              },
              {
                path: 'dashboard',
                component: MainLayoutComponent,
                children: [
                  { path: '', component: DashboardComponent }
                ],
                canActivate:[AuthGuard]
              },
              {
                path:'register',
                component:RegisterComponent
              },
          
]

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
