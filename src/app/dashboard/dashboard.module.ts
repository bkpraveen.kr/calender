import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { CalendarviewComponent } from './calendarview/calendarview.component';
import { TabularviewComponent } from './tabularview/tabularview.component';


@NgModule({
  declarations: [
    CalendarviewComponent,
    TabularviewComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule
  ]
})
export class DashboardModule { }
