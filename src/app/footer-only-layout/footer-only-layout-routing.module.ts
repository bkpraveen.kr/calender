import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
//import { FooterOnlyLayoutComponent } from './footer-only-layout.component';

const routes: Routes = []

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FooterOnlyLayoutRoutingModule { }
