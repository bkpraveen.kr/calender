import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FooterOnlyLayoutRoutingModule } from './footer-only-layout-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    FooterOnlyLayoutRoutingModule
  ]
})
export class FooterOnlyLayoutModule { }
