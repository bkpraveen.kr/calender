import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login.component'
import { FooterOnlyLayoutComponent } from '../footer-only-layout/footer-only-layout.component'
import { RegisterComponent } from '../register/register.component'

const routes: Routes = [{
  //path:'register',
  //component:RegisterComponent
}
 ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule { }
